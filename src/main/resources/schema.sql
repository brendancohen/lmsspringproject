CREATE SCHEMA IF NOT EXISTS bctest;
CREATE TABLE IF NOT EXISTS bctest.emails (
	username char(128) PRIMARY KEY,
	firstName varchar(255),
	lastName varchar(255),
	email varchar(255)
);