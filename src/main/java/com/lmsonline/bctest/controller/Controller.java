package com.lmsonline.bctest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.lmsonline.bctest.data.Data;
import com.lmsonline.bctest.repositories.Repository;

@RestController
@RequestMapping("/bctest")
public class Controller {
	@Autowired
	private Repository repository;

	@RequestMapping(value = "{username}", method = RequestMethod.GET, produces = "application/json")
	public List<Data> dataReturn(@PathVariable String username) {
		return repository.findByUsername(username);
	}

	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public Data dataSave(@RequestBody Data data) throws Exception {
		repository.save(data);
		return data;

	}

}
