package com.lmsonline.bctest.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lmsonline.bctest.data.Data;

public interface Repository extends JpaRepository<Data, String> {
	List<Data> findByUsername(String username);
}
